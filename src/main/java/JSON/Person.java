package JSON;

import java.util.*;

public class Person implements Comparator<Person> {

    private String firstName;
    private String lastName;
    private int age;
    public Person() {

    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String toString() {
        return "Full name: " + firstName + " " +lastName + " Age: " + age;
    }

    public int compare(Person p1, Person p2) {
        return p1.age - p2.age;
    }

    public Person(String n, int a) {
        firstName = n;
        age = a;
    }

}
