package JSON;

import  org.json.JSONArray;
import org.json.JSONObject;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class HTTPRequest {

    public static void main(String[] args) throws IOException {

        String url = "https://pokeapi.co/api/v2/pokemon/";
        URL obj2 = new URL(url);
        HttpURLConnection connection = (HttpURLConnection) obj2.openConnection();

        BufferedReader in = new BufferedReader(
                new InputStreamReader(connection.getInputStream()));
        String inputLine;
        StringBuffer response = new StringBuffer();


        while((inputLine = in.readLine()) != null) {
            response.append(inputLine);
        }
        in.close();

        JSONObject myResponse = new JSONObject(response.toString());

        JSONArray results = new JSONArray(myResponse.get("results").toString());

        for(int i = 0; i < results.length(); i++) {
            System.out.println("Pokemon name: " + results.getJSONObject(i).getString("name")
                    + "\n" + "More info at: " + results.getJSONObject(i).getString("url"));

        }

    }

}
