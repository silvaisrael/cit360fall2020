package JSON;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.core.JsonProcessingException;

public class JSON {

    public static String personToJSON(Person person) {

        ObjectMapper mapper = new ObjectMapper();
        String s = "";

        try {
            s = mapper.writeValueAsString(person);
        } catch (JsonProcessingException e) {
            System.err.println(e.toString());
        }

        return s;
    }

    public static Person JSONToPerson(String s) {

        ObjectMapper mapper = new ObjectMapper();
        Person person = null;

        try {
            person = mapper.readValue(s, Person.class);
        } catch (JsonProcessingException e) {
            System.err.println(e.toString());
        }

        return person;
    }

    public static void main(String[] args) {

        Person person = new Person();
        person.setFirstName("Israel");
        person.setLastName("Silva");
        person.setAge(35);

        String json = JSON.personToJSON(person);
        System.out.println(json);

        Person person2 = JSON.JSONToPerson(json);
        System.out.println(person2);
    }

}
