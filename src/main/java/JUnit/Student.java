package JUnit;

public class Student {

    private String firstName;
    private String lastName;
    private Integer phone;
    private Integer age;

    public Student(){

    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public int getPhone() {
        return phone;
    }

    public void setPhone(int phone) {
        this.phone = phone;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public Student(String f, String l, Integer p, Integer a) {
        firstName = f;
        lastName = l;
        phone = p;
        age = a;
    }

}
