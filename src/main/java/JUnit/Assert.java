package JUnit;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

public class Assert {

    Student student = new Student("Israel", "Silva", 2084197793, 35);
    Student student2 = new Student("Camila", "Goedert", 2084979912, 31);

    @Test
    void notNull() {
        assertNotNull(student);
    }

    @Test
    void notEqual() {
        assertEquals(student.getLastName(), student2.getLastName());
    }

    @Test
    void assertion2() {
        assertEquals(student.getFirstName(), "Israel");
    }

    @Test
    void assertion3() {
        assertEquals(student.getLastName(), "Sylva");
    }

    @Test
    void assertion4() {
        assertEquals(student2.getPhone(), 2084979912);
    }

    @Test
    void assertion5() {
        assertEquals(student2.getAge(), 34);

    }

}
