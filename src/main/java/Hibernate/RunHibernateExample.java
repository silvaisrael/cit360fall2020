package Hibernate;

import java.util.*;

public class RunHibernateExample {

    public static void main(String[] args) {

        TestDAO t = TestDAO.getInstance();

        List<Movie> c = t.getMovies();
        for (Movie i : c) {
            System.out.println(i);
        }

        System.out.println(t.getMovie(1));
        System.out.println(t.getMovie(2));
    }
}
