package FinalProject;

import org.json.JSONArray;
import org.json.JSONObject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.HttpURLConnection;
import java.net.URL;

// This is the Servlet class
@WebServlet(name = "Servlet", urlPatterns = {"/Servlet"})
public class Servlet extends HttpServlet {

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        PrintWriter out = response.getWriter();
        response.setContentType("text/html");
        out.println("<html><head></head><body>");
        out.println("<h1>This is my title</h1>");

        String search = request.getParameter("name");
        search.replaceAll("\\s", "");

        String url = "https://imdb8.p.rapidapi.com/title/find?q=" + search;
        URL obj = new URL(url);
        HttpURLConnection connection = (HttpURLConnection) obj.openConnection();
        connection.setRequestProperty("x-rapidapi-key", "5a1a75b837mshc96df923c53b381p1a1a67jsn185ac6e4c4da");
        connection.setRequestProperty("x-rapidapi-host", "imdb8.p.rapidapi.co");
        BufferedReader in = new BufferedReader(
                new InputStreamReader(connection.getInputStream()));
        String inputLine;
        StringBuffer responseJson = new StringBuffer();

        while((inputLine = in.readLine()) != null) {
            responseJson.append(inputLine);
        }
        in.close();

        int code = connection.getResponseCode();

        JSONObject myResponse = new JSONObject(responseJson.toString());

        JSONArray results = new JSONArray(myResponse.get("results").toString());

        String movieTitle = results.getJSONObject(0).getString("title");

        //PrintWriter out = response.getWriter();
        //response.setContentType("text/html");
        //out.println("<html><head></head><body>");
        //out.println("<h1>This is my title</h1>");

        out.println("<h1>" + movieTitle + "</h1>");

        out.println("<p>My paragraph</p>");
        /*
        for(int i = 0; i < results.length(); i++) {
            out.println("<h1>" + movieTitle + "</h1>");
        }

        out.println("</body></html>");
        /*
        for(int i = 0; i < results.length(); i++) {

            boolean checkTitle = results.getJSONObject(i).keySet().contains("title");
            boolean checkYear = results.getJSONObject(i).keySet().contains("year");
            boolean checkTitleType = results.getJSONObject(i).keySet().contains("titleType");

            // Validation: if result doesn't contain title, year, or titleType, it won't be displayed.
            if(checkTitle && checkYear && checkTitleType) {

                String title = results.getJSONObject(i).getString("title");
                Integer year = results.getJSONObject(i).getInt("year");
                String type = results.getJSONObject(i).getString("titleType");

                //PrintWriter out = response.getWriter();
                response.setContentType("application/json");
                out.println("<html><head></head><body>");

                out.println("<h1>" + title + "</h1>");

                out.println("</body></html>");

            }

        }*/

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

}