package JavaCollections;

import JSON.Person;
import java.util.*;

public class JavaCollections {

    public static void main(String[] args) {

        System.out.println("---This is a LIST---");

        Scanner scanner = new Scanner(System.in);

        List list = new ArrayList();

        Integer counter = 1;

        // This validates whether the string is empty or not
        while(counter < 6) {
            System.out.print("Enter name " + counter + ": ");
            String value = scanner.nextLine();
            if(value.length() > 0) {
                list.add(value);
                counter++;
            } else {
                System.out.println("You must enter a name.");
            }
        }

        System.out.println("Students in this class:");

        list.forEach(n -> {
            System.out.println(n);
        });

        list.remove(2);

        System.out.println("One student dropped the class:");

        list.forEach(n -> {
            System.out.println(n);
        });

        System.out.println("---This is a QUEUE---");

        Queue<Integer> q = new LinkedList<>();

        System.out.println("Enter 5 numbers: ");

        for(int i = 0; i < 5; i++) {
            Integer value = scanner.nextInt();
            q.add(value);
        }

        System.out.println(q);

        q.remove();

        System.out.println("First item is removed with remove() method");

        System.out.println(q);

        q.remove();

        System.out.println("Because a queue is FIFO, first item is removed again");

        System.out.println(q);

        System.out.println("---This is a SET---");

        Set<String> s = new HashSet<>();

        s.add("Peter");
        s.add("James");
        s.add("John");
        s.add("John");

        System.out.println("Because a set can contain no duplicates, the second \"John\" was removed");

        System.out.println(s);

        System.out.println("I also noticed it sorts the list in alphabetical order");

        System.out.println("---This is a TREEMAP---");

        TreeMap<Integer, String> t = new TreeMap();

        t.put(1, "Peter");
        t.put(2, "James");
        t.put(3, "John");
        t.put(4, "Paul");

        System.out.println(t);

        t.remove(2);

        System.out.println("I was able to remove an item using its key");

        System.out.println(t);

        System.out.println("---This is the use of GENERICS---");

        List<Name> name = new LinkedList<>();
        name.add(new Name("Israel", "Silva"));
        name.add(new Name("Camila", "Goedert"));

        name.forEach(n -> {
            System.out.println(n);
        });

        System.out.println("**********");

        List<Person> personList = new ArrayList<Person>();

        personList.add(new Person("Bob", 23));
        personList.add(new Person("Ruth", 15));
        personList.add(new Person("Jake", 6));
        personList.add(new Person("Norman", 89));
        personList.add(new Person("Miles", 38));

        System.out.println("These are the ages unordered:");

        for(Person a: personList)
            System.out.print(a.getAge() + ", ");

        System.out.println(" ");

        Collections.sort(personList, new Person());

        System.out.println("These are the ages in order:");

        for(Person a: personList)
            System.out.print(a.getAge() + ", ");
    }

}
