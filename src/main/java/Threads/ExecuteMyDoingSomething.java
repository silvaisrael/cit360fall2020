package Threads;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class ExecuteMyDoingSomething {

    public static void main(String[] args) {

        ExecutorService myService = Executors.newFixedThreadPool(3);

        MyDoingSomething mds1 = new MyDoingSomething("Bob", 25, 1000);
        MyDoingSomething mds2 = new MyDoingSomething("Ruth", 15, 700);
        MyDoingSomething mds3 = new MyDoingSomething("Jack", 10, 500);
        MyDoingSomething mds4 = new MyDoingSomething("Emily", 5, 200);
        MyDoingSomething mds5 = new MyDoingSomething("Camila", 2, 75);

        myService.execute(mds1);
        myService.execute(mds2);
        myService.execute(mds3);
        myService.execute(mds4);
        myService.execute(mds5);

        myService.shutdown();

    }

}
