package Journal;

import java.util.*;

public class SelfReflectiveJournal {

    public static void main(String[] args) {

        System.out.println("*************************");
        System.out.println("* Welcome to my Journal *");
        System.out.println("*************************" + "\n");

        System.out.print("Enter a week number (between 1 and 13): ");
        Scanner scanner = new Scanner(System.in);
        int week = scanner.nextInt();

        // Validation to make sure user can only enter 1-13
        while(week < 1 || week > 13) {

            System.out.print("You must enter a number between 1 and 13: ");
            week = scanner.nextInt();

        }

        switch (week) {

            case 1:
                System.out.println("Here's what I learned in week " + week + "\n");
                System.out.println(
                        "This week I learned how to install the Java Developer Kit (JDK) " +
                        "and how to add IntelliJ IDEA to write code in Java on my computer. " +
                        "I also participated in a discussion board to get to know my classmates. " + "\n");
                break;

            case 2:
                System.out.println("Here's what I learned in week " + week + "\n");
                System.out.println("Below are a couple of types of collections, to quickly show I learned what they do." + "\n");

                List list = new ArrayList();
                list.add("Anne");
                list.add("Bob");
                list.add("Steph");

                System.out.println("This is a list: " + list);

                Set<String> set = new HashSet<>();
                set.add("Peter");
                set.add("James");
                set.add("John");
                set.add("John");

                System.out.println("This is a Set (Inputs were: Peter, James, John, John - A HashSet sorts items alphabetically, and removes duplicates): " + set);

                break;

            case 3:
                System.out.println("Here's what I learned in week " + week + "\n");
                System.out.println("I loved learning about Exception handling with try/catch blocks " +
                        "because I never really learned about it in a meaningful way before. " +
                        "Now I understand that they can solve problems that might arise later, " +
                        "and deal with them if they happen without the need to debug code and add more things later." + "\n");

                System.out.println("Example (An array contains 3 items. I try to print the fourth index): ");

                try {
                    int[] numbers = {2, 3, 4};
                    System.out.println(numbers[3]);
                } catch (Exception e) {
                    System.out.println("That didn't work!");
                }

                break;

            case 4:
                System.out.println("Here's what I learned in week " + week + "\n");
                System.out.println("This week I learned about JSON and HTTP requests. " +
                        "HTTP requests are used to fetch information from an online source, like an API. " +
                        "The information comes back in the JSON format, which can be displayed to the user. " +
                        "I loved using the SWAPI (Star Wars API) and the POKEAPI (Pokemon API) to practice."+ "\n");
                break;

            case 5:
                System.out.println("Here's what I learned in week " + week + "\n");
                System.out.println("This week I learned about JUNIT and how unit tests can be useful. " +
                        "I struggled with it a bit at first, but then I figured out that its main use " +
                        "is for testing user input or other things before the program is deployed. " +
                        "It can simulate input, among other things, and run automatically as many times as needed." + "\n");
                break;

            case 6:
                System.out.println("Here's what I learned in week " + week + "\n");
                System.out.println("Use Case documents are important because they show the bare bones of a project. " +
                        "A good Use Case document is written after thoughtful considerations and as such " +
                        "should include things that might be overlooked, like a plan B of rif an API call fails." + "\n");
                break;

            case 7:
                System.out.println("Here's what I learned in week " + week + "\n");
                System.out.println("What to say about Servlets. I like the idea of being able to start a Web server using TomCat. " +
                        "But when I tried displaying content from an API call using servlets, my program would not work. " +
                        "I spent countless hours reading forums, watching videos, and asking people about my errors, and all for nothing. " +
                        "I can make simple HTML show in the browser, but I will not give up and end up learning how to display the results of " +
                        "an HTTP request response from JSON. Mark my words!" + "\n");
                break;

            case 8:
                System.out.println("Here's what I learned in week " + week + "\n");
                System.out.println("In week 8, I practiced running an application with multiple threads to have " +
                        "concurrent things run at the same time, thus speeding long processes. " + "\n");
                break;

            case 9:
                System.out.println("Here's what I learned in week " + week + "\n");
                System.out.println("Hibernate to me sounds a lot like C#'s Entity Framework. " +
                        "With Hibernate I can have a Java object that is a bridge between a database and my Java classes. "+ "\n");
                break;

            case 10:
                System.out.println("Here's what I learned in week " + week + "\n");
                System.out.println("Documentation is not one of my strong suits. I don't think it is any developer's strong suit. " +
                        "But after week 10, I can honestly say that creating UML Sequence Diagrams can make development much smoother. " +
                        "It allows me to see how a user will interact with the application and what steps need to happen " +
                        "for the user to get what he or she requested." + "\n");
                break;

            case 11:
                System.out.println("Here's what I learned in week " + week + "\n");
                System.out.println("This week was when we proposed what application we would be creating for our final project. " +
                        "I realized that more often than not, the final product will not be what I had envisioned. " +
                        "Especially because I proposed my app before starting to plan for it, so once planning started, " +
                        "I realized that some of the things I wanted were just too difficult or even impossible. " + "\n");
                break;

            case 12:
                System.out.println("Here's what I learned in week " + week + "\n");
                System.out.println("After developing my app for a few days, I realized I was not going to be able to deliver " +
                        "what I promised using the technology I had chosen (Servlets). I decided to move to a console app " +
                        "and am glad the final product turned out great. I learned that it is important to adapt and know " +
                        "when to give up and try something else. It was almost too late, but I am glad I didn't insist " +
                        "on hitting my head against a brick wall. " + "\n");
                break;

            case 13:
                System.out.println("Here's what I learned in week " + week + "\n");
                System.out.println("In this last week of the semester and of developing my final project in Java " +
                        "I can say with (a little) confidence, that I can develop in Java. I also learned that once " +
                        "you know how to code, learning new languages becomes easier, as I now know JavaScript, " +
                        "Java, C# and more. " + "\n");
                break;

        }

    }

}
