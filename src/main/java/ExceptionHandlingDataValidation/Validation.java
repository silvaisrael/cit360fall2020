package ExceptionHandlingDataValidation;

import java.text.DecimalFormat;
import java.util.Scanner;

public class Validation {

    public static void main(String[] args) {

        int value1, value2;
        float result;
        Scanner scan = new Scanner(System.in);
        DecimalFormat df = new DecimalFormat("#.##");

        System.out.print("Enter first number: ");
        value1 = scan.nextInt();
        System.out.print("Enter second number: ");
        value2 = scan.nextInt();

        try {

            result = divide(value1, value2);

            // Takes care of result being infinity
            if (result == Double.POSITIVE_INFINITY || result == Double.NEGATIVE_INFINITY) {
                throw new ArithmeticException();
            }

            System.out.println("The result of " + value1 + " divided by " + value2 + " is: " + df.format(result));

        } catch (ArithmeticException e) {

            System.out.println("You cannot divide a number by 0.");
            System.out.print("Enter second number (not 0): ");
            value2 = scan.nextInt();

            result = divide(value1, value2);

            System.out.println("The result of " + value1 + " divided by " + value2 + " is: " + df.format(result));
        }

        /*
        if (value2 == 0) {
            System.out.println("You cannot divide a number by 0.");
            System.out.print("Enter second number (not 0): ");
            value2 = scan.nextInt();
        }

        result = divide(value1, value2);

        System.out.println("The result of " + value1 + " divided by " + value2 + " is: " + df.format(result));
        */

    }

    public static float divide(int num1, int num2) {
        float calc;
        calc = (float)num1 / num2;

        return calc;
    }
}